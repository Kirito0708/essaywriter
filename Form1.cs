﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace EssayHelp
{
    public partial class Form1 : Form
    {
        System.Timers.Timer gui_timer = new System.Timers.Timer(500);

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
              
        }

        private void Main_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sentence = "";
            Random r = new Random();
            Manager mn = new Manager();
            Sentence_History s = new Sentence_History();

            if (langlvl.Value == 0) 
            {
                mn.gather_sentence(s.object_existance_beginner);
                mn.gather_sentence(s.object_ach_beginner);
            }

            //Mixture of both 0 & 2
            else if (langlvl.Value == 1)
            {
                int idx = r.Next(0, 2);
                switch (idx) 
                {
                    case 0:
                        mn.gather_sentence(s.object_existance_beginner);
                        break;
                    case 1:
                        mn.gather_sentence(s.object_existance_intermediate);
                        break;
                }

                idx = r.Next(0, 2);
                switch (idx) 
                {
                    case 0:
                        mn.gather_sentence(s.object_ach_beginner);
                        break;
                    case 1:
                        mn.gather_sentence(s.object_ach_intermediate);
                        break;
                }
            }

            else if (langlvl.Value == 2)
            {
                mn.gather_sentence(s.object_existance_intermediate);
                mn.gather_sentence(s.object_ach_intermediate);
            }

            for (int i = 0; i < mn.sentence.Count; i++) 
            {
                mn.sentence[i] = mn.sentence[i].Replace("$name", ename.Text);
                mn.sentence[i] = mn.sentence[i].Replace("$race", e_race.Text);
                mn.sentence[i] = mn.sentence[i].Replace("$role", e_role.Text);
                mn.sentence[i] = mn.sentence[i].Replace("$y1", textBox1.Text.ToString());
                mn.sentence[i] = mn.sentence[i].Replace("$y2", textBox2.Text.ToString());
                mn.sentence[i] = mn.sentence[i].Replace("$achyr", textBox5.Text.ToString());
                mn.sentence[i] = mn.sentence[i].Replace("$ach", textBox3.Text);

                string personalpronoun;
                switch (listBox1.Text) 
                {
                    case "Male":
                        personalpronoun = "his";
                        break;
                    case "Female":
                        personalpronoun = "her";
                        break;
                    default:
                        personalpronoun = "its";
                        break;
                }
                mn.sentence[i] = mn.sentence[i].Replace("$ppnoun", personalpronoun);

                string thirdpronoun;

                switch (listBox1.Text)
                {
                    case "Male":
                        thirdpronoun = "he";
                        break;
                    case "Female":
                        thirdpronoun = "she";
                        break;
                    default:
                        thirdpronoun = "it";
                        break;
                }

                mn.sentence[i] = mn.sentence[i].Replace("$tpnoun", thirdpronoun);

                sentence += mn.sentence[i];
            }

            textBox4.Text = sentence;
            sentence = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(textBox4.Text.ToString())) 
            {
                MessageBox.Show("Generate an essay first!");
                return;
            }

            if (!File.Exists("essay.txt")) { File.Create("essay.txt"); Thread.Sleep(1000); }

            StreamWriter sw = new StreamWriter("essay.txt");
            sw.WriteLine(textBox4.Text.ToString());
            sw.Flush();
            sw.Dispose();
            sw.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(textBox4.Text);
        }
    }
}

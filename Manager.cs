﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EssayHelp
{
    public class Manager
    {
        public List<string> sentence = new List<string>();
        public void gather_sentence(string[] arrstr) 
        {
            Random r = new Random();
            sentence.Add(arrstr[r.Next(0, arrstr.Length)]);
        }

        public string beginners_essay_template = 
            "$name was born in the year $y1 and died in the year $y2. " +
            "$name was a $race $role that $acc.";
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EssayHelp
{
    public class Sentence_History
    {
        public string[] object_ach_intermediate = 
        {
            "In the year $achyr, $name $ach. ",
            "In the year $achyr, $tpnoun $ach. ",
            "$tpnoun $name $ach in $achyr. "
        };

        public string[] object_ach_beginner = 
        {
            "$name $ach in the year $achyr. ",
            "$tpnoun $ach in the year $achyr. "
        };

        public string[] object_existance_intermediate = 
        {
            "$name, was a $race $role who was born in the year $y1, and died in the year $y2. ",
            "$name was born in the year $y1. $name was a $race $role until $ppnoun death in the year $y2. ",
            "$name was born during the year $y1. $tpnoun died in the year $y2. ",
            "$name was a $race $role. "
        };

        public string[] object_existance_beginner =
        {
            "$name was born in the year $y1 and died in the year $y2. ",
            "$name was born in the year $y1. $tpnoun died in the year $y2. ",
            "$name was born in the year $y1, died in the year $y2. ",
            "$name was born in $y1, and died in $y2."
        };
    }
}
